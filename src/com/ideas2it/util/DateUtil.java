package com.ideas2it.util;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.apache.log4j.Logger;

import com.ideas2it.common.Constant;
import com.ideas2it.exception.AppException;
import com.ideas2it.logger.LoggerInfo;

/**
 * <p>
 * Caluates the age of a person
 * </p>
 */
public class DateUtil {

    /**
     * <p>
     * Caluates the age of a person
     * </p>
     * @param dateOfBirth           Birth date of person
     * @returns age of the person 
     */
    public static int calculateAge(String dateOfBirth) throws AppException {
        int age = 0;
        try {
            LocalDate today = LocalDate.now();
            LocalDate birthDate = LocalDate.parse(dateOfBirth,
                              DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            Period diff = Period.between(birthDate, today); 
            age = diff.getYears();
            if (age < 0) {
             throw new AppException(Constant.FUTURE_YEAR);
            }
            return age;
        } catch (DateTimeParseException e) {
            LoggerInfo.error(DateUtil.class, Constant.PARSE_DATE_ERROR 
                                + dateOfBirth, e);
        } 
        return age;
    }
}
