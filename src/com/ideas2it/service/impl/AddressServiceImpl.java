package com.ideas2it.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.dao.AddressDao;
import com.ideas2it.dao.impl.AddressDaoImpl;
import com.ideas2it.exception.AppException;
import com.ideas2it.model.Address;
import com.ideas2it.service.AddressService;
import com.ideas2it.util.CommonUtil;

/**
 * <p>
 * Perform searching, updating, displaying, deleting operations on address 
 * </p>
 * @author Selvakumar
 * @Created on 13/07/2017
 */
public class AddressServiceImpl implements AddressService {
    private AddressDao addressDao = new AddressDaoImpl();
                
    /**
     * <p>
     * @see com.ideas2it.service.AddressService;
     * #boolean createAddress(String doorNo, String streetName,
     *                           String landMark,String city, String postalCode, 
     *                           String district, String state, String country,
     *                           String Id) throws AppException
     * </p>    
     */
    public boolean createAddress (String doorNo, String streetName, 
                                String landMark, String city, String postalCode,
                                String district, String state, String country,
                                String id) throws AppException {              
        Address address = new Address (doorNo, streetName, landMark, city, 
                                          postalCode, district, state, country);                
        return addressDao.insertAddress(address, id);   
    }

    /**
     * <p>
     * @see com.ideas2it.service.AddressService;
     * #boolean changeAddress(String doorNo, String streetName,
     *                           String landMark,String city, String postalCode, 
     *                           String district, String state, String country,
     *                           String Id) throws AppException
     * </p>     
     */
    public boolean changeAddress (String doorNo, String streetName, 
                                String landMark, String city, String postalCode,
                                String district, String state, String country,
                                String id) throws AppException {              
        Address address = new Address (doorNo, streetName, landMark, city, 
                                          postalCode, district, state, country);                
        return addressDao.updateAddress(address, id);   
    }
    
    /**
     * <p>
     * @see com.ideas2it.service.AddressService;
     * #boolean removeAddressById(String Id)
     * </p>
     */          
    public boolean removeAddressById(String id) throws AppException {
        if (isValidId(id)) {
            return addressDao.deleteAddress(id);
        }
        return false;
    }    

    /**
     * <p>
     * @see com.ideas2it.service.AddressService;
     * #boolean searchAddressById(String Id)
     * </p>
     */          
    public List<Address> searchAddressById(String id) throws AppException {
        if (isValidId(id)) { 
            return addressDao.getAddress(id);
        }
        return null;
    }
    
    /**
     * <p>
     * Check the id type
     * </p>
     * @returns true if the id is in valid format
     */
    private boolean isValidId(String id) {
        if (!CommonUtil.isValidId(id)) {
            throw new AppException("Invalid employee Id");
        }
        return true;
    }    
}
