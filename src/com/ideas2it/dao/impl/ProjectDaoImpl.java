package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ideas2it.common.Constant;
import com.ideas2it.connection.DataBaseConnection;
import com.ideas2it.dao.ProjectDao;
import com.ideas2it.exception.AppException;
import com.ideas2it.logger.LoggerInfo;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
 
public class ProjectDaoImpl implements ProjectDao {
    private static final String PROJECT_ID = "PROJECT_ID";
    private static final String PROJECT_NAME = "PROJECT_NAME";
    private static final String PROJECT_DOMAIN = "PROJECT_DOMAIN";
    private static final String EMP_ID = "EMPLOYEE_ID";
    private static final String EMP_NAME = "EMPLOYEE_NAME";
    private static final String EMP_MAIL = "EMPLOYEE_MAIL";
    private static final String EMP_DOB = "EMPLOYEE_DOB";
    private static final String EMP_DESIGNATION = "EMPLOYEE_DESIGNATION";     
    private static Connection connection;

    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public void insertProject(Project project)
     * </p>
     */
    public void insertProject(Project project) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                String sql = "INSERT INTO PROJECT(" + PROJECT_ID + "," 
                                                     + PROJECT_NAME + "," 
                                                     + PROJECT_DOMAIN + ")"
                                                     +  "VALUES(?,?,?)";
                PreparedStatement statement =  connection.prepareStatement(sql);
                statement.setString(1, project.getId());
                statement.setString(2, project.getName());
                statement.setString(3, project.getDomain());            
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                                Constant.INSERTION_ERROR + project,
                                exception);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public Project getProject(String projectId)
     * </p>
     */    
    public Project getProject(String projectId) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement =
                         connection.prepareStatement("SELECT " + PROJECT_ID
                                                   + "," + PROJECT_NAME + "," 
                                                   + PROJECT_DOMAIN 
                                                   + " FROM PROJECT " 
                                                   + "WHERE " + PROJECT_ID 
                                                   + " = ?");
                statement.setString(1, projectId);
                ResultSet resultSet = statement.executeQuery( );
                while (resultSet.next()) {
                    Project project = new Project(
                                          resultSet.getString(PROJECT_ID), 
                                          resultSet.getString(PROJECT_NAME),
                                          resultSet.getString(PROJECT_DOMAIN));
                    return project;
                }
                resultSet.close();
                statement.close();
            } 
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                                Constant.PROJECT_FETCHING_ERROR + projectId,
                                exception);
            throw new AppException();
        } finally {
           closeConnection();
        }
        return null;
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public void deleteProject(String projectId)
     * </p>
     */
    public void deleteProject(String projectId) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = 
                              connection.prepareStatement("UPDATE EMPLOYEE SET " 
                                                    + PROJECT_ID + " = null"
                                                    + " WHERE " 
                                                    + PROJECT_ID + "= ?");
                statement.setString(1, projectId);
                statement.executeUpdate();        
                statement =  connection.prepareStatement("DELETE FROM PROJECT "
                                                    + "WHERE " + PROJECT_ID
                                                    + " = ?");
                statement.setString(1, projectId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                                Constant.PROJECT_DELETION_ERROR  + projectId,
                                exception);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public void assignProject(String projectId, String employeeId)
     * </p>
     */    
    public void assignProjectToEmployee(String projectId, String employeeId) 
                                            throws AppException {        
        try {
            getConnection();
            if (null != connection) {
                String query = "UPDATE EMPLOYEE SET " + PROJECT_ID + " = ?" 
                                   + " WHERE " + EMP_ID + " = ?";
                PreparedStatement statement = 
                                             connection.prepareStatement(query);   
                statement.setString(1, projectId);
                statement.setString(2, employeeId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                                Constant.EMPLOYEE_PROJECT_ASSIGNMENT_ERROR  
                                + projectId 
                                + Constant.PROJECT_ASSIGNING_EMPLOYEE_ID
                                + employeeId);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public void deleteEmployeeFromProject(String projectId,
     *                                       String employeeId)
     * </p>
     */
    public void deleteEmployeeFromProject(String projectId, String employeeId)
                                    throws AppException {
        try {
            getConnection();
            if (null != connection) {
                String query = "UPDATE EMPLOYEE SET " + PROJECT_ID + " = ?" 
                                   + " WHERE " + EMP_ID + " = ?";
                PreparedStatement statement = 
                                             connection.prepareStatement(query);   
                statement.setString(1, null);
                statement.setString(2, employeeId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                               Constant.EMPLOYEE_REMOVING_ERROR_FROM_PROJECT
                               + projectId 
                               + Constant.EMPLOYEE_ID_TO_BE_REMOVED_FROM_PROJECT  
                               + employeeId);
           throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public void updateProjectName(String projectId, String newName)
     * </p>
     */ 
    public void updateProjectName(String projectId, String newName)
                                    throws AppException {
        updateProject(PROJECT_NAME, projectId, newName);
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public void updateProjectDomain(String projectId, String newDomain)
     * </p>
     */    
    public void updateProjectDomain(String projectId, String newDomain)
                                   throws AppException {       
        updateProject(PROJECT_DOMAIN, projectId, newDomain);
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public void updateProject(String tableColumnName, String projectId, 
     *                           String dataToBeUpdated)
     * </p>
     */    
    public void updateProject(String tableColumnName, String projectId, 
                               String dataToBeUpdated) 
                               throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = 
                                         connection.prepareStatement("UPDATE "
                                                  + "PROJECT SET " 
                                                  + "? = ? WHERE " + PROJECT_ID
                                                  + " = ?");   
                statement.setString(1, tableColumnName);
                statement.setString(2, dataToBeUpdated);
                statement.setString(3, projectId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, Constant.UPDATION_ERROR 
                            + dataToBeUpdated
                            + Constant.UPDATING_COLUMN + tableColumnName  
                            + Constant.USING_PROJECT_ID + projectId);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public List<Project> retrieveProjects()
     * </p>
     */    
    public List<Project> retrieveProjects() throws AppException {
        try {   
            getConnection();
            if (null != connection) {
                List<Project> projects = new ArrayList<Project>();
                PreparedStatement statement =  
                                connection.prepareStatement("SELECT "
                                                           + PROJECT_ID + ","
                                                           + PROJECT_NAME + ","
                                                           + PROJECT_DOMAIN
                                                           + " FROM "
                                                           + "PROJECT");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Project project = new Project(
                                          resultSet.getString(PROJECT_ID),
                                          resultSet.getString(PROJECT_NAME),
                                          resultSet.getString(PROJECT_DOMAIN));
                    projects.add(project);
                }
                resultSet.close();
                statement.close();
                return projects;
            }
            return null;
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                                Constant.PROJECTS_FETCHING_ERROR, exception);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public List<Employee> getEmployeesInProject(String projectId)
     * </p>
     */   
    public List<Employee> getEmployeesInProject(String projectId) 
                                    throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement preparedStatement = 
                                        connection.prepareStatement("SELECT * "
                                                      + "FROM EMPLOYEE WHERE " 
                                                      + PROJECT_ID + " = ? ");
                preparedStatement.setString(1, projectId);
                ResultSet resultSet = preparedStatement.executeQuery();
                List<Employee> employees = new ArrayList<Employee>();
                while (resultSet.next()) {
                    Employee employee = new Employee(
                                           resultSet.getString(EMP_ID),  
                                           resultSet.getString(EMP_DESIGNATION),
                                           resultSet.getString(EMP_MAIL),
                                           resultSet.getString(EMP_NAME),
                                           resultSet.getString(EMP_DOB));
                    employees.add(employee);
                }
                resultSet.close();
                preparedStatement.close();
                return employees;
            }
            return null;
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, Constant.EMPLOYEES_IN_PROJECT 
                            + projectId, exception);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * <p>
     * @See com.ideas2it.dao.ProjectDao;
     * #public List<List<String>> getEmployeesNotInProject()
     * </p>
     */    
    public List<List<String>> getEmployeesNotInProject() throws AppException {
        try {
            getConnection();
            if (null != connection) {
                List<List<String>> employees = new ArrayList<List<String>>();
                List<String> employee = null;
                PreparedStatement statement = 
                                        connection.prepareStatement("SELECT " 
                                                            + EMP_ID + "," 
                                                            + EMP_NAME + ","
                                                            + EMP_MAIL 
                                                            + " FROM "
                                                            + "EMPLOYEE WHERE " 
                                                            + PROJECT_ID 
                                                            + " IS "
                                                            + "NULL");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    employee = new ArrayList<String>();
                    employee.add(resultSet.getString(EMP_ID));
                    employee.add(resultSet.getString(EMP_MAIL));
                    employee.add(resultSet.getString(EMP_NAME));
                    employees.add(employee);
                }
                resultSet.close();
                statement.close();
                return employees;
            }
            return null;
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                                 Constant.EMPLOYEES_NOT_IN_PROJECT, exception);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * <p>
     * Getting Connection
     * </p>
     */
    private void getConnection() {
        try {
            DataBaseConnection dataBaseConnection =  
                                              DataBaseConnection.getInstance();
            connection = dataBaseConnection.getConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                                Constant.CONNECTION_ESTABLISH_ERROR, exception);        
        }
    }
    
    /**
     * <p>
     * Closing Connection
     * </p>
     */    
    private void closeConnection() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                                Constant.CONNECTION_CLOSE_ERROR, exception);        
             throw new AppException();
        }
    }
}

