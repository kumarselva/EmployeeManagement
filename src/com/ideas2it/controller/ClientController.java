package com.ideas2it.controller;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.common.Constant;
import com.ideas2it.exception.AppException;
import com.ideas2it.model.Address;
import com.ideas2it.model.Client;
import com.ideas2it.service.AddressService;
import com.ideas2it.service.ClientService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.service.impl.AddressServiceImpl;
import com.ideas2it.service.impl.ClientServiceImpl;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.view.ClientView;

/**
 * <p>
 * Control the flow of operations between the view and service class
 * </p> 
 */
public class ClientController {
    private ClientService clientService = new ClientServiceImpl();
      
    /**
     * <p>
     * Creating the the client detail by passing the information to the 
     * service class
     * </p>
     * @param clientId                Id number of client used for 
     *                                searching, deleting and displaying the 
     *                                particular client detail
     * @param clientName              Name of client
     * @param clientMail              MailId of client and it is unique
     * @param clientOrganisation      Organisation name of client
     */
    public boolean createClient(String clientId, String clientName, 
                                    String clientMail, 
                                    String clientOrganisation) {
        ClientView clientView = new ClientView();
        try {
             return clientService.createClient(clientId, clientName, clientMail, 
                                                   clientOrganisation); 
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
        return false;
    }
    
    /**
     * <p>
     * Creating the the address of client by passing the information to the 
     * service class
     * </p>
     * @param doorNo                    door number of client
     * @param street                    Street name of client
     * @param landmark                  Important place near to the client's
     *                                  living area 
     * @param city                      Name of city
     * @param postalCode                Postal number of the city
     * @param district                  Name of the district where the person is
     *                                  living
     * @param state                     Name of the state where the person is
     *                                  living
     * @param country                   Name of the country where the person is 
     *                                  currently living
     * @param clientId                  Client id number used for searching,
     *                                  removing and displaying the client 
     *                                  detail
     */
    public boolean createAddress(String doorNo, String street, String  landMark, 
                                 String city, String postalCode,
                                 String district, String state,
                                 String country, String clientId) {
        AddressService addressService = new AddressServiceImpl();
        ClientView clientView = new ClientView();
        try {
            return addressService.createAddress(doorNo, street, landMark, city, 
                                             postalCode, district, state, 
                                             country,clientId);
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
        return false;
    }

    /**
     * <p>
     * Changing the the address of client by passing the information to the 
     * service class
     * </p>
     * @param doorNo                    Door number which replace the current 
     *                                  door number of client
     * @param street                    New street name of client
     * @param landmark                  Important place near to the client's
     *                                  living area 
     * @param city                      Name of city
     * @param postalCode                Postal number of the city
     * @param district                  Name of the district where the person is
     *                                  living
     * @param state                     Name of the state where the person is
     *                                  living
     * @param country                   Name of the country where the person is 
     *                                  currently living
     * @param clientId                Client id number used for searching,
     *                                  removing and displaying the client 
     *                                  detail
     */
    public boolean changeAddress(String doorNo, String street, String  landMark, 
                                 String city, String postalCode,
                                 String district, String state,
                                 String country, String clientId) {
        AddressService addressService = new AddressServiceImpl();
        ClientView clientView = new ClientView();
        try {
            return addressService.changeAddress(doorNo, street, landMark, city, 
                                             postalCode, district, state, 
                                             country,clientId);
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
        return false;
    }
     
    /**
     * <p>
     * searching client profile by using the client Id
     * </p>
     * @param clientId                Id number of client used for 
     *                                  searching and displaying the 
     *                                  particular client detail
     */   
    public Client getClientById(String clientId){
        ClientView clientView = new ClientView();
        try {
            return clientService.searchClientById(clientId);
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
        return null;
    }    

    /**
     * <p>
     * searching address detail of client by using the client Id
     * </p>
     * @param clientId                Id number of client used for 
     *                                  searching and displaying the 
     *                                  particular client detail
     */   
    public List<Address> getAddressById(String clientId){
        AddressService addressService = new AddressServiceImpl();
        ClientView clientView = new ClientView();
        try {
            return addressService.searchAddressById(clientId);
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
        return null;
    }    
    
    /**
     * <p>
     * Removing client profile using client Id
     * </p>
     * @param clientId                Id number of client used for 
     *                                  searching and deleting the 
     *                                  particular client detail
     */
    public boolean removeClientById(String clientId) {
        AddressService addressService = new AddressServiceImpl();
        ClientView clientView = new ClientView();
        try {
            return (addressService.removeAddressById(clientId) && 
                       clientService.removeClientById(clientId));
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
        return false;
    }
            
    /**
     * <p>
     * Dispaly all client details with all information which includes client
     * name, Id number, mail id .
     * </p>
     */
    public List<Client> getClients() {
        ClientView clientView = new ClientView();
        try {
            return clientService.getClients();
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
        return null;
    }

    /**
     * <p>
     * Changes the client information includes client name, Id number, mail id.
     * </p>
     * @param clientId                Id number of client used for modifying,
     *                                searching, deleting and displaying the 
     *                                particular client detail
     * @param clientName              Name of client
     * @param clientMail              MailId of client and it is unique
     * @param clientOrganisation      Organisation name of client
     */    
    public void changeClientInformation(String clientId, String clientName, 
                                             String clientMail, 
                                             String clientOrganisation) {       
        ClientView clientView = new ClientView();
        try {
            clientService.changeClientInfo(clientId, clientName, clientMail, 
                                              clientOrganisation);
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
    }
    
    /**
     * <p>
     * Assigning the project detail to the particular client
     * </p>
     * @param clientId                Id number of client used for 
     *                                searching, deleting and displaying the 
     *                                particular client detail
     * @param projectId               Id number of project used for searching,
     *                                deleting,displaying the particular project
     *                                detail
     * @param projectName             Name of the project
     * @param projectDomain           Domain name of project
     */
    public boolean assignProjectToClient(String clientId, String projectId, 
                                            String projectName, 
                                            String projectDomain) {
        ClientView clientView = new ClientView();
        ProjectService projectService = new ProjectServiceImpl();
        try {
             projectService.createProject(projectId, projectName,  
                                                    projectDomain);
             clientService.assignProjectToClient(clientId, projectId); 
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
        return false;
    }

        
    /**
     * <p>
     * Findout the presence of client using the client Id
     * </p>
     * @param clientId       Id number of client to confirm presence
     * @returns true if the client Id exists
     */
    public boolean isClientPresent(String clientId) {
        ClientView clientView = new ClientView();
        try {
            return clientService.isClientPresent(clientId);
        } catch (AppException exception) {
            clientView.printMessage(exception.getMessage());
        }
        return false;
    }                   
}
