package com.ideas2it.model;

import java.util.Calendar;
/**
 * Project details
 * @author Selvakumar
 * Date 07/07/2017
 */
public class Project {
    private String id;
    private String name;
    private String domain;
  
    public Project (String id, String name, String domain) {                 
        this.id = id;
        this.name = name;
        this.domain = domain;
    }
    
    public String getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDomain() {
        return domain;
    }
   
    public void setId(String id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setDomain(String domain) {
        this.domain = domain;
    }
        
    public String toString() {
        return  "\nProject Id               : " + id
                    + "\nProject Name             : " + name
                    + "\nProject Domain           : " + domain +"\n";
    }
}
