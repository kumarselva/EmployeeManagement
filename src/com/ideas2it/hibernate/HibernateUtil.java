package com.ideas2it.hibernate;
 
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * <p>
 * Singleton class to access the the session factory object
 * </p>
 */ 
public class HibernateUtil {
    private static HibernateUtil instance = null;
    private static SessionFactory sessionFactory;
    private static ServiceRegistryBuilder registry;
   
    /**
     * <p>
     * Constructor of singleton class used to instantiate the object of session
     * factory class
     * </p>
     */
    private HibernateUtil() {
        Configuration configuration = new Configuration();
        configuration.configure();
        registry = new ServiceRegistryBuilder().
                           applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = registry.buildServiceRegistry();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }
    
    /**
     * <p>
     * Creates the instance for the class if the instance for the class is null
     * </p>
     * @returns the instance of the class
     */ 
    public static HibernateUtil getInstance(){
        if(null == instance) {
            instance = new HibernateUtil();
        }
        return instance;
    }

    /**
     * <p>
     * Gets the sessionfactory object
     * </p>
     * @returns the sessionFactory object
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * <p>
     * Closes the sessionfactory 
     * </p>
     */
    public static void closeSessionFactory() {
        sessionFactory.close();
    }
}
