package com.ideas2it.model;

/**
 * Client details
 * @author Selvakumar
 * Date 07/07/2017
 */
public class Client {
    private String id;
    private String name;
    private String mail;
    private String organisation;
    
    public Client (String id, String name, String mail, 
                      String organisation) {                 
        this.id = id;
        this.name = name;
        this.mail = mail;
        this.organisation = organisation;
    }

    public String getId() {
        return id;
    }
            
    public String getName() {
        return name;
    }
        
    public String getMail() {
        return mail;
    }
   
    public String getOrganisation() {
        return organisation;
    }
    

    public void setId(String id) {
        this.id = id;
    }
   
    public void setName(String name) {
        this.name = name;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }
    public String toString() {
        return "\nClient Name           : " + name
                   + "\nClient Mail           : " + mail
                   + "\nClient Organisation   : " + organisation +"\n";
    }
}
