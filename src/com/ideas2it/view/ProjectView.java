package com.ideas2it.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.common.Constant;
import com.ideas2it.controller.ProjectController;

/**
 * <p>
 * Getting all the details from user
 * </p>
 * @author Selvakumar
 * Created 12/07/2017
 */
public class ProjectView {
    private Scanner scan = new Scanner(System.in);
    private ProjectController projectController = new ProjectController();
    
    /**
     * <p>
     * Performs the certain operation on project whatever the user wants to
     * perform from the shown detail like Creating creating,searching,changing,
     * removing and displaying the project detail and also user can performs 
     * assigining project to particular employee, gets the employees who wre not 
     * in any projects.
     * </p>
     */
    public void manageProject() {
        boolean flag = true;
        try {  
            do {
                System.out.println(Constant.USER_OPINION_PROJECT);
                int condition = scan.nextInt();
                switch (condition) {
                    case 1:
                        createProject(); 
                        break;
                    case 2:
                        searchProjectById();
                        break;      
                    case 3:
                        removeProjectById();
                        break;
                    case 4:
                        showProjects();
                        break;
                    case 5:
                        updateProject();
                        break;
                    case 6:
                        assignProjectToEmployee();
                        break;
                    case 7:
                        removeEmployeeFromProject();
                        break;
                    case 8:
                        showEmployeesInProject();
                        break;
                    case 9:
                        showEmployeesNotInProject();
                        break;
                    default:
                        flag = false;
                        break;
                }
            } while (flag);       
        } catch (InputMismatchException e) {
            System.out.println(Constant.INVALID_INPUT);
        }
        scan.nextLine();
    }   
    
    /**
     * <p>
     * Displays the message which is passed as a parameter
     * </p>
     * @param message         The information which is to be displayed
     */
    public void printMessage(String message) {
        System.out.println(message);
    }                    

    /**
     * <p>
     * Creating an project by getting the informations about the project like 
     * project Id,name,domain name.
     * </p>
     */
    private void createProject() {
        System.out.println(Constant.PROJECT_ID);
        String projectId = scan.next();
        System.out.println(Constant.PROJECT_NAME);
        String projectName = scan.next();
        System.out.println(Constant.PROJECT_DOMAIN);
        String projectDomain = scan.next();
        projectController.createProject(projectId, projectName, projectDomain);
    }
    
    /**
     * <p> 
     * Searching the project detail by using project ID number
     * </p>
     */   
    private void searchProjectById() {
        System.out.println(Constant.SEARCH_PROJECT);
        String projectId = scan.next();
        projectController.searchProjectById(projectId);
    }
    
    /**
     * <p> 
     * Searching the project detail by using project ID number
     * </p>
     */     
    private void removeProjectById() {
        System.out.println(Constant.DELETE_PROJECT);
        String projectId = scan.next();
        projectController.removeProjectById(projectId);
    }

    /**
     * <p>
     * Displays all the projects detail which includes project name,project Id,
     * domain name.
     * </p> 
     */            
    private void showProjects() {
        projectController.getProjects();
    }
    
    /**
     * <p>
     * Updating the project detail by using the project ID. If the ID exists,
     * user can perform changing operation on project name,domain name.
     * If the ID doesn't exist, user can't update the information.
     * </p>
     */
    private void updateProject() {
        try {
            System.out.println(Constant.PROJECT_ID);
            String projectId = scan.next();
            if (projectController.isProjectPresent(projectId)) {  
                boolean flag = true;
                do {
                    System.out.println(Constant.UPDATE_PROJECT);
                    int condition = scan.nextInt();
                    switch (condition) {
                        case 1:
                            changeName(projectId);
                            break;
                        case 2:
                            changeDomain(projectId);
                            break;
                        default :
                            flag = false;
                            break;
                    }
                } while (flag); 
            } else {
                System.out.println(Constant.PROJECT_ID_NOT_FOUND);
            } 
        } catch (InputMismatchException e) {
            System.out.println(Constant.INVALID_INPUT);
        }   
    }
    
    /**
     * <p>
     * Changes the name of project corresponding to the project ID
     * </p>
     * @param projectId      Id number of project used for the name changing
     *                       purpose
     */
    private void changeName(String projectId) {
        System.out.println(Constant.NEW_PROJECT_NAME);
        String newName = scan.next(); 
        projectController.changeName(projectId, newName);
     }
    

    /**
     * <p>
     * Changes the domain name of project corresponding to the project ID
     * </p>
     * @param projectId      Id number of project used for the domain name 
     *                       changing purpose
     */
    private void changeDomain(String projectId) {
        System.out.println(Constant.NEW_PROJECT_DOMAIN);
        String newDomain = scan.next();
        projectController.changeDomain(projectId, newDomain);
    }
    
    /**
     * <p>
     * Assigning project to employee by using the project id and employee id
     * </p>
     */
    private void assignProjectToEmployee() {
        System.out.println(Constant.PROJECT_ID);
        String projectId = scan.next();
        System.out.println(Constant.EMPLOYEE_ID);
        String employeeId = scan.next();
        System.out.println(Constant.EMPLOYEE_DESIGNATION);
        String employeeDesignation = scan.next();
        System.out.println(Constant.EMPLOYEE_MAIL);
        String employeeMailId = scan.next();
        System.out.println(Constant.EMPLOYEE_NAME);
        String employeeName = scan.next();
        System.out.println(Constant.EMPLOYEE_DOB);
        String employeeDob = scan.next();
        projectController.assignProjectToEmployee(projectId, employeeId,
                                                   employeeDesignation, 
                                                   employeeMailId,
                                                   employeeName,
                                                   employeeDob);
    }
    
    /**
     * <p>
     * Removing employee from project by using the project id and employee id
     * </p>
     */
    private void removeEmployeeFromProject() {
        System.out.println(Constant.PROJECT_ID);
        String projectId = scan.next();
        System.out.println(Constant.EMPLOYEE_ID);
        String employeeId = scan.next();
        projectController.removeEmployeeFromProject(projectId, employeeId); 
    }
     
    /**
     * <p>
     * Shows the employees detail assigned to the particular project by using
     * the project Id
     * </p>
     */    
    private void showEmployeesInProject() {
        System.out.println(Constant.PROJECT_ID);
        String projectId = scan.next();
        projectController.getEmployeesInProject(projectId);
    }
    
    /**
     * <p>
     * Shows the employees detail who are not assigned to any project
     * </p>
     */    
    private void showEmployeesNotInProject() {
        projectController.getEmployeesNotInProject();
    }    
}
