package com.ideas2it.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * <p>
 * Performs singleton object creation, establishing connection with database
 * and close the connection with database
 * </p>
 * @author Selvakumar
 * @created on 26/07/2017
 */
public class DataBaseConnection {
    private static Connection connection;
    private static DataBaseConnection dbConnection ;
    private static final String URL = "jdbc:mysql://localhost/"
                                 + "IDEAS2IT?autoReconnect=true&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "root";
     
    /**
     * <p>
     * Connects to the database and returns the connected object
     * </p>
     * @returns the  connection object that holds connection to database
     */    
    public Connection getConnection() throws SQLException {        
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            return connection;
        } catch (SQLException exception) {
            throw exception;
        }
    }
    
    /**
     * <p>
     * Creation of singleton object
     * </p>
     * @returns connection object to the calling method or class
     */
    public static synchronized DataBaseConnection getInstance() {
        if ( null == dbConnection) {
            synchronized (DataBaseConnection.class) {
                if (null == dbConnection) {
                    dbConnection = new DataBaseConnection();
                }
            }
        }
        return dbConnection;
    }

    /**
     * <p>
     * Closes the connection with database
     * </p>
     */     
    public static void closeConnection() throws SQLException{
        try {
            connection.close();
        } catch (SQLException exception) {
            throw exception;
        }
    }

    /**
     * <p>
     * Empty constructor
     * </p>
     */     
    private DataBaseConnection() {
    }
}
