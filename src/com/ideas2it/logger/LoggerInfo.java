package com.ideas2it.logger;

import java.io.File;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * <p>
 * This  class creates a logger for application logging
 * </p>
 */
public class LoggerInfo {
   private static Logger logger;
   
    /**
     * <p>
     * Creates the Logger with the class Name appended
     * </p>
     */
    public static void setLogger(Object obj) {        
        logger = Logger.getLogger(obj.getClass());
        DOMConfigurator.configure(System.getProperty("user.dir")
                                     + "/log4j.xml");            
    }
        
    /**
     * <p>
     * It sends the debug message to the logger
     * </p>
     * @param message 
     */
    public static void debug(Object obj, String message) {
        setLogger(obj);
        logger.debug(message);
    }

    /**
     * <p>
     * It sends the debug message to the logger
     * </p>
     * @param message 
     */
    public static void debug(Object obj, String message, Throwable cause) {
        setLogger(obj);
        logger.debug(message, cause);
    }

    /**
     * <p>
     * It sends the information to the logger
     * </p>
     * @param message   Contains iformation which is to be shown in the logger
     */
    public static void info(Object obj, String message) {
        setLogger(obj);
        logger.info(message);
    }
    
    /**
     * <p>
     * It sends the warn message to the logger
     * </p>
     * @param message   Contains warning message 
     */
    public static void warn(Object obj, String message) {
        setLogger(obj);
        logger.warn(message);
    }    
    
    /**
     * <p>
     * It sends the error message to the logger
     * </p>
     * @param message  Contains error information
     */
    public static void error(Object obj, String message) {
        setLogger(obj);
        logger.error(message);
    }    
        
    /**
     * <p>
     * It sends the error message to the logger
     * </p>
     * @param message  Contains error information
     */
    public static void error(Object obj, String message, Throwable cause) {
        setLogger(obj);
        logger.error(message, cause);
    }    
}
