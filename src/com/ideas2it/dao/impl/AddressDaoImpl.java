package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ideas2it.common.Constant;
import com.ideas2it.connection.DataBaseConnection;
import com.ideas2it.dao.AddressDao;
import com.ideas2it.exception.AppException;
import com.ideas2it.logger.LoggerInfo;
import com.ideas2it.model.Address;
 
public class AddressDaoImpl implements AddressDao { 
    private static final String DOOR_NO = "DOOR_NO";
    private static final String STREET = "STREET";
    private static final String LAND_MARK = "LAND_MARK";
    private static final String CITY = "CITY";
    private static final String POSTAL_CODE = "POSTAL_CODE";
    private static final String DISTRICT = "DISTRICT";
    private static final String STATE = "STATE";
    private static final String COUNTRY = "COUNTRY";
    private static final String CLIENT_ID = "CLIENT_ID";
    private static final String EMPLOYEE_ID = "EMPLOYEE_ID";
    private Connection connection = null;

    /**
     * <p>
     * @com.ideas2it.dao.AddressDao//MySQLIntegrityConstraintViolationException:
     * #public void insertAddress(Address address, String Id)
     * </p>
     */
    public boolean insertAddress(Address address, String id) 
                                                      throws AppException {        
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = null;
                String idType[] = id.split("-");
                if (idType[0].equals("E") ) {
                    String sql = "INSERT INTO ADDRESS(" + DOOR_NO + "," + STREET 
                                        + "," + LAND_MARK + "," + CITY + ","
                                        + POSTAL_CODE + "," + DISTRICT 
                                        + "," + STATE + "," + COUNTRY + "," 
                                        + EMPLOYEE_ID + ")" 
                                        + " VALUES(?,?,?,?,?,?,?,?,?)";
                    statement =  connection.prepareStatement(sql);                                                  
                } else if (idType[0].equals("C")) {
                    String sql = "INSERT INTO ADDRESS(" + DOOR_NO + "," + STREET 
                                        + "," + LAND_MARK + "," + CITY + ","
                                        + POSTAL_CODE + "," + DISTRICT 
                                        + "," + STATE + "," + COUNTRY + "," 
                                        + CLIENT_ID + ")" 
                                        + " VALUES(?,?,?,?,?,?,?,?,?)";
                    statement =  connection.prepareStatement(sql);
               }
                statement.setString(1, address.getDoorNo());
                statement.setString(2, address.getStreetName());
                statement.setString(3, address.getLandMark());           
                statement.setString(4, address.getCity());
                statement.setString(5, address.getPostalCode());
                statement.setString(6, address.getDistrict());
                statement.setString(7, address.getState());
                statement.setString(8, address.getCountry());
                statement.setString(9, id);
                statement.executeUpdate();
                statement.close();
                return true;
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(AddressDaoImpl.class, Constant.INSERTION_ERROR
                                     + address + Constant.USING_ID + id );
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }
        return false;
    }
    
    /**
     * <p>
     * @com.ideas2it.dao.AddressDao
     * #public void updateAddress(Address address, String Id)
     * </p>
     */       
    public boolean updateAddress(Address address, String id)
                                throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = null;
                String idType[] = id.split("-");
                if (idType[0].equals("E") ) {
                    statement = connection.prepareStatement("UPDATE "
                                                + "ADDRESS SET " 
                                                + DOOR_NO + " = ?,"
                                                + STREET + " = ? ,"
                                                + LAND_MARK + " = ?,"
                                                + CITY + " = ?,"
                                                + POSTAL_CODE + " = ?,"
                                                + DISTRICT + " = ?,"
                                                + STATE + " = ?,"
                                                + COUNTRY + " = ? "
                                                +"WHERE " + EMPLOYEE_ID 
                                                + "= ?");
                } else if (idType[0].equals("C") ) {
                    statement = connection.prepareStatement("UPDATE "
                                                + "ADDRESS SET " 
                                                + DOOR_NO + " = ?,"
                                                + STREET + " = ? ,"
                                                + LAND_MARK + " = ?,"
                                                + CITY + " = ?,"
                                                + POSTAL_CODE + " = ?,"
                                                + DISTRICT + " = ?,"
                                                + STATE + " = ?,"
                                                + COUNTRY + " = ? "
                                                +"WHERE " + CLIENT_ID 
                                                + "= ?");
                }
                statement.setString(1, address.getDoorNo());
                statement.setString(2, address.getStreetName());
                statement.setString(3, address.getLandMark());           
                statement.setString(4, address.getCity());
                statement.setString(5, address.getPostalCode());
                statement.setString(6, address.getDistrict());
                statement.setString(7, address.getState());
                statement.setString(8, address.getCountry());
                statement.setString(9, id);
                statement.executeUpdate();
                statement.close();
                return true;
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(AddressDaoImpl.class, Constant.UPDATION_ERROR 
                                + address + Constant.USING_ID+ id);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }
        return false;    
    }
    
    /**
     * <p>
     * @com.ideas2it.dao.AddressDao
     * #public void deleteAddress(String employeeId)
     * </p>
     */    
    public boolean deleteAddress(String id) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                 PreparedStatement statement = null;
                 String idType[] = id.split("-");
                 if (idType[0].equals("E") ) {
                     statement = connection.prepareStatement("DELETE FROM " 
                                                           + "ADDRESS "
                                                           + "WHERE " 
                                                           + EMPLOYEE_ID
                                                           + " = ?");
                 } else if (idType[0].equals("C") ) {
                     statement = connection.prepareStatement("DELETE FROM " 
                                                           + "ADDRESS "
                                                           + "WHERE " 
                                                           + CLIENT_ID
                                                           + " = ?");
                 }
                 statement.setString(1, id);
                 statement.executeUpdate();
                 statement.close();
                 return true;
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(AddressDaoImpl.class, Constant.DELETION_ERROR + id,
                                    exception);                                   
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }
        return false;    
    }

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * #public Address getAddress(String Id)
     * </p>
     */    
    public List<Address> getAddress(String id) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                  PreparedStatement statement = null;
                   List<Address> addresses = null;
                String idType[] = id.split("-");
                if (idType[0].equals("E") ) {
                    addresses = new ArrayList<Address>();
                    statement = connection.prepareStatement("SELECT "
                                                + DOOR_NO + ","
                                                + STREET + ","
                                                + LAND_MARK + ","
                                                + CITY + ","
                                                + POSTAL_CODE + ","
                                                + DISTRICT + ","
                                                + STATE + ","
                                                + COUNTRY + " FROM ADDRESS "
                                                +"WHERE " + EMPLOYEE_ID 
                                                + " = ?");
                } else if (idType[0].equals("C") ) {
                    addresses = new ArrayList<Address>();
                    statement = connection.prepareStatement("SELECT "
                                                + DOOR_NO + ","
                                                + STREET + ","
                                                + LAND_MARK + ","
                                                + CITY + ","
                                                + POSTAL_CODE + ","
                                                + DISTRICT + ","
                                                + STATE + ","
                                                + COUNTRY + " FROM ADDRESS "
                                                +"WHERE " + CLIENT_ID 
                                                + " = ?");
                }
                statement.setString(1, id);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                     Address address = new Address(
                                        resultSet.getString(DOOR_NO),
                                        resultSet.getString(STREET),
                                        resultSet.getString(LAND_MARK),           
                                        resultSet.getString(CITY),
                                        resultSet.getString(POSTAL_CODE),
                                        resultSet.getString(DISTRICT),
                                        resultSet.getString(STATE),
                                        resultSet.getString(COUNTRY));
                    addresses.add(address);
                }
                resultSet.close();
                statement.close();
                return addresses;
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(AddressDaoImpl.class, Constant.FETCHING_ERROR 
                              + id);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }     
        return null;
    }

    /**
     * <p>
     * Getting Connection
     * </p>
     */
    private void getConnection() {
        try {
            DataBaseConnection dataBaseConnection =  
                                              DataBaseConnection.getInstance();
            connection = dataBaseConnection.getConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(AddressDaoImpl.class, 
                                Constant.CONNECTION_ESTABLISH_ERROR, exception);        
        }
    }
    
    /**
     * <p>
     * Closing Connection
     * </p>
     */    
    private void closeConnection() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(AddressDaoImpl.class,
                                Constant.CONNECTION_CLOSE_ERROR, exception);
             throw new AppException(Constant.NO_CONNECTION);
        }
    }  
}
