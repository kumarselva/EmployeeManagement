package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.common.Constant;
import com.ideas2it.connection.DataBaseConnection;
import com.ideas2it.dao.EmployeeDao;
import com.ideas2it.exception.AppException;
import com.ideas2it.hibernate.HibernateUtil;
import com.ideas2it.logger.LoggerInfo;
import com.ideas2it.model.Employee;
 
public class EmployeeDaoImpl implements EmployeeDao {
    private final String empId = "EMPLOYEE_ID";
    private final String empName = "EMPLOYEE_NAME";
    private final String empMail = "EMPLOYEE_MAIL";
    private final String empDob = "EMPLOYEE_DOB";
    private final String empDesignation = "EMPLOYEE_DESIGNATION";
    private Connection connection;

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * void insertEmployee(Employee employee)
     * </p>
     */
    public void insertEmployee(Employee employee) throws AppException {        
        HibernateUtil hibernateUtil = HibernateUtil.getInstance();
        SessionFactory factory = hibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction transaction = null;    
        try {
            transaction = session.beginTransaction();
            session.save(employee); 
            transaction.commit();
        } catch (HibernateException e) {
             if (transaction!=null) transaction.rollback();
                 LoggerInfo.debug(EmployeeDaoImpl.class, 
                                     Constant.INSERTION_ERROR + employee, e);
                 throw new AppException(Constant.NO_CONNECTION); 
        } finally {
             session.close();
             hibernateUtil.closeSessionFactory(); 
        }
   }

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * Employee getEmployee(String employeeId)
     * </p>
     */    
     public Employee getEmployee(String employeeId) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = 
                       connection.prepareStatement("SELECT " + empId + "," 
                                                  + empName + "," + empDob + "," 
                                                  + empDesignation + "," 
                                                  + empMail + " FROM "
                                                  + "EMPLOYEE WHERE " + empId
                                                  + " = ?");
                statement.setString(1, employeeId);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Employee employee = new Employee(
                                           resultSet.getString(empId),  
                                           resultSet.getString(empDesignation),
                                           resultSet.getString(empMail),
                                           resultSet.getString(empName),
                                           resultSet.getString(empDob));
                    return employee;
                }
                resultSet.close();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, 
                                Constant.EMPLOYEE_FETCHING_ERROR + employeeId, 
                                exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }     
        return null;
    }

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * void deleteEmployee(String employeeId)
     * </p>
     */    
    public void deleteEmployee(String employeeId) throws AppException {
       try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = 
                           connection.prepareStatement("DELETE FROM EMPLOYEE "
                                                           + "WHERE " + empId
                                                           + " = ?");
                statement.setString(1, employeeId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, 
                                Constant.EMPLOYEE_DELETION_ERROR+ employeeId,
                                exception);                                   
               exception.printStackTrace();
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }    
    }

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * void updateEmployeeName(String employeeId, String newName)
     * </p>
     */
    public void updateEmployeeName(String employeeId, String newName)
                                     throws AppException {
        updateEmployee(empName, employeeId, newName);
    }

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * void updateEmployeeDesignation(String employeeId, 
     *                                        String newDesignation)
     * </p>
     */    
    public void updateEmployeeDesignation(String employeeId, 
                                             String newDesignation)
                                             throws AppException {
        updateEmployee(empDesignation, employeeId, newDesignation);
    }

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * # public void updateEmployeeMail(String employeeId, String newMail)
     * </p>
     */    
   public void updateEmployeeMail(String employeeId, String newMail) 
                                   throws AppException {
        updateEmployee(empMail, employeeId, newMail);
    }

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * void updateEmployeeDob(String employeeId, String newDob)
     * </p>
     */
    public void updateEmployeeDob(String employeeId, String newDob) 
                                     throws AppException {
        updateEmployee(empDob, employeeId, newDob);   
    }

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * void updateEmployee(String tableColumnName, 
     *                               String employeeId, String dataToBeUpdated)
     * </p>
     */       
    public void updateEmployee(String tableColumnName, 
                                String employeeId, String dataToBeUpdated)
                                throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement =  
                                  connection.prepareStatement("UPDATE "
                                                              + "EMPLOYEE SET " 
                                                              + "? = ? "
                                                              +"WHERE "
                                                              + empId 
                                                              + "= ?");  
                statement.setString(1,tableColumnName);
                statement.setString(2, dataToBeUpdated);
                statement.setString(3, employeeId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, Constant.UPDATION_ERROR 
                            + dataToBeUpdated
                            + Constant.UPDATING_COLUMN + tableColumnName  
                            + Constant.USING_ID + employeeId, exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }    
    }

    /**
     * <p>
     * @com.ideas2it.dao.EmployeeDao
     * List<Employee> retrieveEmployees()
     * </p>
     */    
    public List<Employee> retrieveEmployees() throws AppException {
        try {
            getConnection();
            if (null != connection) {
                List<Employee> employees = new ArrayList<Employee>();
                PreparedStatement statement = 
                             connection.prepareStatement("SELECT " 
                                                             + empId + "," 
                                                             + empName + "," 
                                                             + empDob + "," 
                                                             + empDesignation 
                                                             + "," + empMail 
                                                             + " FROM "
                                                             + "EMPLOYEE");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Employee employee = new Employee(
                                           resultSet.getString(empId),  
                                           resultSet.getString(empDesignation),
                                           resultSet.getString(empMail),
                                           resultSet.getString(empName),
                                           resultSet.getString(empDob));
                    employees.add(employee);
                }
                resultSet.close();
                statement.close();
                return employees;
            }
            return null;
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, 
                                Constant.EMPLOYEE_FETCHING_ERROR, exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }
    }
    /**
     * <p>
     * Getting Connection
     * </p>
     */
    private void getConnection() {
        try {
            DataBaseConnection dataBaseConnection =  
                                              DataBaseConnection.getInstance();
            connection = dataBaseConnection.getConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, 
                                Constant.CONNECTION_ESTABLISH_ERROR, exception);        
        }
    }
    
    /**
     * <p>
     * Closing Connection
     * </p>
     */    
    private void closeConnection() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {        
            LoggerInfo.debug(EmployeeDaoImpl.class, 
                                Constant.CONNECTION_CLOSE_ERROR, exception);        
            throw new AppException(Constant.NO_CONNECTION);
        }
    }      
}

